��2�      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��target���)��}�(h�).. _overview-of-primitives-and-pipelines:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��$overview-of-primitives-and-pipelines�u�tagname�h
�line�K�parent�hhh�source��'X:\d3m2\docs\core_concepts\overview.rst�ubh	�section���)��}�(hhh]�(h	�title���)��}�(h�$Overview of Primitives and Pipelines�h]�h	�Text����$Overview of Primitives and Pipelines�����}�(hh,h h*hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h h%hhh!h"hKubh	�	paragraph���)��}�(h��Let's start with basic definitions of the two main concepts of the D3M program. For a higher-level overview, see :ref:`the-d3m-architecture`.�h]�(h/�sLet’s start with basic definitions of the two main concepts of the D3M program. For a higher-level overview, see �����}�(h�qLet's start with basic definitions of the two main concepts of the D3M program. For a higher-level overview, see �h h<hhh!NhNubh �pending_xref���)��}�(h�:ref:`the-d3m-architecture`�h]�h	�inline���)��}�(hhIh]�h/�the-d3m-architecture�����}�(hhh hMubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhKh hGubah}�(h]�h]�h]�h]�h]��refdoc��core_concepts/overview��	refdomain�hX�reftype��ref��refexplicit���refwarn���	reftarget��the-d3m-architecture�uhhEh!h"hKh h<ubh/�.�����}�(h�.�h h<hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKh h%hhubh$)��}�(hhh]�(h))��}�(h�
Primitives�h]�h/�
Primitives�����}�(hh|h hzhhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h hwhhh!h"hK
ubh;)��}�(hX�  Primitives are the basic building blocks of the D3M ecosystem. They are typically used as the steps in D3M *pipelines*, which we define below.
Primitives generally represent *learnable functions* or models, whose logic don't need to be explicitly pre-defined (or coded) but rather can be **learned** from inputs and outputs.
A special case of learnable functions are "regular" functions with pre-defined logic and execution steps.�h]�(h/�kPrimitives are the basic building blocks of the D3M ecosystem. They are typically used as the steps in D3M �����}�(h�kPrimitives are the basic building blocks of the D3M ecosystem. They are typically used as the steps in D3M �h h�hhh!NhNubh	�emphasis���)��}�(h�*pipelines*�h]�h/�	pipelines�����}�(hhh h�ubah}�(h]�h]�h]�h]�h]�uhh�h h�ubh/�8, which we define below.
Primitives generally represent �����}�(h�8, which we define below.
Primitives generally represent �h h�hhh!NhNubh�)��}�(h�*learnable functions*�h]�h/�learnable functions�����}�(hhh h�ubah}�(h]�h]�h]�h]�h]�uhh�h h�ubh/�_ or models, whose logic don’t need to be explicitly pre-defined (or coded) but rather can be �����}�(h�] or models, whose logic don't need to be explicitly pre-defined (or coded) but rather can be �h h�hhh!NhNubh	�strong���)��}�(h�**learned**�h]�h/�learned�����}�(hhh h�ubah}�(h]�h]�h]�h]�h]�uhh�h h�ubh/�� from inputs and outputs.
A special case of learnable functions are “regular” functions with pre-defined logic and execution steps.�����}�(h�� from inputs and outputs.
A special case of learnable functions are "regular" functions with pre-defined logic and execution steps.�h h�hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKh hwhhubh$)��}�(hhh]�(h))��}�(h�Implementing Primitives�h]�h/�Implementing Primitives�����}�(hh�h h�hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h h�hhh!h"hKubh;)��}�(hX�  We write primitives in Python by extending a suitable base class and additional mixins (as necessary).
The underlying logic for primitives can be written in any language, as long as it conforms to D3M's Python wrapper interface designed to expose the logic in a standard way regardless of language. For more detail on writing your own primitives, see the :doc:`../quickstart` after reading through this overview.�h]�(h/Xe  We write primitives in Python by extending a suitable base class and additional mixins (as necessary).
The underlying logic for primitives can be written in any language, as long as it conforms to D3M’s Python wrapper interface designed to expose the logic in a standard way regardless of language. For more detail on writing your own primitives, see the �����}�(hXc  We write primitives in Python by extending a suitable base class and additional mixins (as necessary).
The underlying logic for primitives can be written in any language, as long as it conforms to D3M's Python wrapper interface designed to expose the logic in a standard way regardless of language. For more detail on writing your own primitives, see the �h h�hhh!NhNubhF)��}�(h�:doc:`../quickstart`�h]�hL)��}�(hh�h]�h/�../quickstart�����}�(hhh h�ubah}�(h]�h]�(hW�std��std-doc�eh]�h]�h]�uhhKh h�ubah}�(h]�h]�h]�h]�h]��refdoc�hd�	refdomain�h��reftype��doc��refexplicit���refwarn��hj�../quickstart�uhhEh!h"hKh h�ubh/�% after reading through this overview.�����}�(h�% after reading through this overview.�h h�hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKh h�hhubeh}�(h]��implementing-primitives�ah]�h]��implementing primitives�ah]�h]�uhh#h hwhhh!h"hKubh$)��}�(hhh]�(h))��}�(h�Key Ingredients of a Primitive�h]�h/�Key Ingredients of a Primitive�����}�(hj&  h j$  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h j!  hhh!h"hKubh;)��}�(h�|A primitive generally has the following elements (though not necessarily all of them, depending on primitive type/function):�h]�h/�|A primitive generally has the following elements (though not necessarily all of them, depending on primitive type/function):�����}�(hj4  h j2  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKh j!  hhubh	�enumerated_list���)��}�(hhh]�(h	�	list_item���)��}�(h�Primitive metadata�h]�h;)��}�(hjI  h]�h/�Primitive metadata�����}�(hjI  h jK  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKh jG  ubah}�(h]�h]�h]�h]�h]�uhjE  h jB  hhh!h"hNubjF  )��}�(h� Parameters (state) being learned�h]�h;)��}�(hj`  h]�h/� Parameters (state) being learned�����}�(hj`  h jb  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKh j^  ubah}�(h]�h]�h]�h]�h]�uhjE  h jB  hhh!h"hNubjF  )��}�(h�Hyperparameters�h]�h;)��}�(hjw  h]�h/�Hyperparameters�����}�(hjw  h jy  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKh ju  ubah}�(h]�h]�h]�h]�h]�uhjE  h jB  hhh!h"hNubjF  )��}�(h�*Structural type for the primitive's inputs�h]�h;)��}�(hj�  h]�h/�,Structural type for the primitive’s inputs�����}�(hj�  h j�  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKh j�  ubah}�(h]�h]�h]�h]�h]�uhjE  h jB  hhh!h"hNubjF  )��}�(h�+Structural type for the primitive's outputs�h]�h;)��}�(hj�  h]�h/�-Structural type for the primitive’s outputs�����}�(hj�  h j�  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKh j�  ubah}�(h]�h]�h]�h]�h]�uhjE  h jB  hhh!h"hNubjF  )��}�(h�DImplementation of required methods, i.e., the *primitive interface*
�h]�h;)��}�(h�CImplementation of required methods, i.e., the *primitive interface*�h]�(h/�.Implementation of required methods, i.e., the �����}�(h�.Implementation of required methods, i.e., the �h j�  ubh�)��}�(h�*primitive interface*�h]�h/�primitive interface�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhh�h j�  ubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKh j�  ubah}�(h]�h]�h]�h]�h]�uhjE  h jB  hhh!h"hNubeh}�(h]�h]�h]�h]�h]��enumtype��arabic��prefix�h�suffix�hpuhj@  h j!  hhh!h"hKubeh}�(h]��key-ingredients-of-a-primitive�ah]�h]��key ingredients of a primitive�ah]�h]�uhh#h hwhhh!h"hKubh$)��}�(hhh]�(h))��}�(h�$Exploring the D3M Primitives Library�h]�h/�$Exploring the D3M Primitives Library�����}�(hj�  h j�  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h j�  hhh!h"hK"ubh;)��}�(hX�  To avoid re-inventing the wheel, it's a good idea to take a look at the primitives that have been developed by other contributors.
One way to do it is to use `MARVIN <https://marvin.datadrivendiscovery.org/primitives>`_, where you can search and filter for different primitives based on problem type, family, keywords, etc. For a higher-level overview of all the different teams who have contributed, see the `Primitives <https://datadrivendiscovery.org/primitives>`_ section on the D3M home page.�h]�(h/��To avoid re-inventing the wheel, it’s a good idea to take a look at the primitives that have been developed by other contributors.
One way to do it is to use �����}�(h��To avoid re-inventing the wheel, it's a good idea to take a look at the primitives that have been developed by other contributors.
One way to do it is to use �h j  hhh!NhNubh	�	reference���)��}�(h�=`MARVIN <https://marvin.datadrivendiscovery.org/primitives>`_�h]�h/�MARVIN�����}�(h�MARVIN�h j  ubah}�(h]�h]�h]�h]�h]��name�j  �refuri��1https://marvin.datadrivendiscovery.org/primitives�uhj  h j  ubh)��}�(h�4 <https://marvin.datadrivendiscovery.org/primitives>�h]�h}�(h]��marvin�ah]�h]��marvin�ah]�h]��refuri�j   uhh
�
referenced�Kh j  ubh/��, where you can search and filter for different primitives based on problem type, family, keywords, etc. For a higher-level overview of all the different teams who have contributed, see the �����}�(h��, where you can search and filter for different primitives based on problem type, family, keywords, etc. For a higher-level overview of all the different teams who have contributed, see the �h j  hhh!NhNubj  )��}�(h�:`Primitives <https://datadrivendiscovery.org/primitives>`_�h]�h/�
Primitives�����}�(h�
Primitives�h j4  ubah}�(h]�h]�h]�h]�h]��name�j<  j  �*https://datadrivendiscovery.org/primitives�uhj  h j  ubh)��}�(h�- <https://datadrivendiscovery.org/primitives>�h]�h}�(h]��id2�ah]�h]��
primitives�ah]�h]��refuri�jD  uhh
j.  Kh j  ubh/� section on the D3M home page.�����}�(h� section on the D3M home page.�h j  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK$h j�  hhubh)��}�(h�.. _pipeline-intro:�h]�h}�(h]�h]�h]�h]�h]�h�pipeline-intro�uhh
hK'h j�  hhh!h"ubeh}�(h]��$exploring-the-d3m-primitives-library�ah]�h]��$exploring the d3m primitives library�ah]�h]�uhh#h hwhhh!h"hK"ubeh}�(h]��
primitives�ah]�h]�h]�jN  ah]�uhh#h h%hhh!h"hK
j.  Kubh$)��}�(hhh]�(h))��}�(h�	Pipelines�h]�h/�	Pipelines�����}�(hj|  h jz  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h jw  hhh!h"hK*ubh	�image���)��}�(h�C.. image:: ../images/pipeline.png
  :width: 600px
  :align: center
�h]�h}�(h]�h]�h]�h]�h]��width��600px��align��center��uri��$core_concepts\../images/pipeline.png��
candidates�}��*�j�  suhj�  h jw  hhh!h"hNubh;)��}�(h��A *pipeline* is basically a series of interconnected steps that are executed in order
to solve a particular *problem* (such as prediction based on historical
data).�h]�(h/�A �����}�(h�A �h j�  hhh!NhNubh�)��}�(h�
*pipeline*�h]�h/�pipeline�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhh�h j�  ubh/�` is basically a series of interconnected steps that are executed in order
to solve a particular �����}�(h�` is basically a series of interconnected steps that are executed in order
to solve a particular �h j�  hhh!NhNubh�)��}�(h�	*problem*�h]�h/�problem�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhh�h j�  ubh/�/ (such as prediction based on historical
data).�����}�(h�/ (such as prediction based on historical
data).�h j�  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK0h jw  hhubh;)��}�(h��Pipelines have data-flow semantics, meaning that steps are not necessarily executed in the order they appear in the pipeline definition (more on this :doc:`here <pipeline_example>`)�h]�(h/��Pipelines have data-flow semantics, meaning that steps are not necessarily executed in the order they appear in the pipeline definition (more on this �����}�(h��Pipelines have data-flow semantics, meaning that steps are not necessarily executed in the order they appear in the pipeline definition (more on this �h j�  hhh!NhNubhF)��}�(h�:doc:`here <pipeline_example>`�h]�hL)��}�(hj�  h]�h/�here�����}�(hhh j�  ubah}�(h]�h]�(hW�std��std-doc�eh]�h]�h]�uhhKh j�  ubah}�(h]�h]�h]�h]�h]��refdoc�hd�	refdomain�j�  �reftype��doc��refexplicit���refwarn��hj�pipeline_example�uhhEh!h"hK4h j�  ubh/�)�����}�(h�)�h j�  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK4h jw  hhubh;)��}�(hX/  A step of a pipeline is usually a *primitive* (a step can be
something else, however, like a sub-pipeline, but for the purposes of
this tutorial, assume that each step is a primitive): something that
individually could, for example, transform data into another format, or
fit a model for prediction. There are many types of primitives (see the
`primitives index repo`_ for the full
list of available primitives). In a pipeline, the steps must be arranged
in a way such that each step must be able to read the data in the format
produced by the preceding step.�h]�(h/�"A step of a pipeline is usually a �����}�(h�"A step of a pipeline is usually a �h j  hhh!NhNubh�)��}�(h�*primitive*�h]�h/�	primitive�����}�(hhh j  ubah}�(h]�h]�h]�h]�h]�uhh�h j  ubh/X+   (a step can be
something else, however, like a sub-pipeline, but for the purposes of
this tutorial, assume that each step is a primitive): something that
individually could, for example, transform data into another format, or
fit a model for prediction. There are many types of primitives (see the
�����}�(hX+   (a step can be
something else, however, like a sub-pipeline, but for the purposes of
this tutorial, assume that each step is a primitive): something that
individually could, for example, transform data into another format, or
fit a model for prediction. There are many types of primitives (see the
�h j  hhh!NhNubj  )��}�(h�`primitives index repo`_�h]�h/�primitives index repo�����}�(h�primitives index repo�h j"  ubah}�(h]�h]�h]�h]�h]��name��primitives index repo�j  �1https://gitlab.com/datadrivendiscovery/primitives�uhj  h j  �resolved�Kubh/�� for the full
list of available primitives). In a pipeline, the steps must be arranged
in a way such that each step must be able to read the data in the format
produced by the preceding step.�����}�(h�� for the full
list of available primitives). In a pipeline, the steps must be arranged
in a way such that each step must be able to read the data in the format
produced by the preceding step.�h j  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK7h jw  hhubh)��}�(h�L.. _primitives index repo: https://gitlab.com/datadrivendiscovery/primitives�h]�h}�(h]��primitives-index-repo�ah]�h]��primitives index repo�ah]�h]�j  j3  uhh
hKAh jw  hhh!h"j.  Kubh;)��}�(hXw  For this tutorial, let's try to use the example pipeline that comes with
a primitive called
``d3m.primitives.classification.logistic_regression.SKlearn`` to predict
baseball hall-of-fame players, based on their stats (see the
`185_baseball dataset <https://datasets.datadrivendiscovery.org/d3m/datasets/-/tree/master/training_datasets/seed_datasets_archive/185_baseball>`__).�h]�(h/�^For this tutorial, let’s try to use the example pipeline that comes with
a primitive called
�����}�(h�\For this tutorial, let's try to use the example pipeline that comes with
a primitive called
�h jL  hhh!NhNubh	�literal���)��}�(h�=``d3m.primitives.classification.logistic_regression.SKlearn``�h]�h/�9d3m.primitives.classification.logistic_regression.SKlearn�����}�(hhh jW  ubah}�(h]�h]�h]�h]�h]�uhjU  h jL  ubh/�I to predict
baseball hall-of-fame players, based on their stats (see the
�����}�(h�I to predict
baseball hall-of-fame players, based on their stats (see the
�h jL  hhh!NhNubj  )��}�(h��`185_baseball dataset <https://datasets.datadrivendiscovery.org/d3m/datasets/-/tree/master/training_datasets/seed_datasets_archive/185_baseball>`__�h]�h/�185_baseball dataset�����}�(h�185_baseball dataset�h jj  ubah}�(h]�h]�h]�h]�h]��name��185_baseball dataset�j  �xhttps://datasets.datadrivendiscovery.org/d3m/datasets/-/tree/master/training_datasets/seed_datasets_archive/185_baseball�uhj  h jL  ubh/�).�����}�(h�).�h jL  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKCh jw  hhubh;)��}�(hX1  Let's take a look at the example pipeline. Many example pipelines can be found
in `primitives index repo`_ where they demonstrate how to use particular primitives.
At the time of this writing, an example pipeline can be found `here
<https://gitlab.com/datadrivendiscovery/primitives/blob/master/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines/862df0a2-2f87-450d-a6bd-24e9269a8ba6.json>`__,
but this repository's directory names and files periodically change, so it is
prudent to see how to navigate to this file too.�h]�(h/�TLet’s take a look at the example pipeline. Many example pipelines can be found
in �����}�(h�RLet's take a look at the example pipeline. Many example pipelines can be found
in �h j�  hhh!NhNubj  )��}�(h�`primitives index repo`_�h]�h/�primitives index repo�����}�(h�primitives index repo�h j�  ubah}�(h]�h]�h]�h]�h]��name��primitives index repo�j  j3  uhj  h j�  j4  Kubh/�x where they demonstrate how to use particular primitives.
At the time of this writing, an example pipeline can be found �����}�(h�x where they demonstrate how to use particular primitives.
At the time of this writing, an example pipeline can be found �h j�  hhh!NhNubj  )��}�(h��`here
<https://gitlab.com/datadrivendiscovery/primitives/blob/master/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines/862df0a2-2f87-450d-a6bd-24e9269a8ba6.json>`__�h]�h/�here�����}�(h�here�h j�  ubah}�(h]�h]�h]�h]�h]��name�j�  j  ��https://gitlab.com/datadrivendiscovery/primitives/blob/master/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines/862df0a2-2f87-450d-a6bd-24e9269a8ba6.json�uhj  h j�  ubh/��,
but this repository’s directory names and files periodically change, so it is
prudent to see how to navigate to this file too.�����}�(h��,
but this repository's directory names and files periodically change, so it is
prudent to see how to navigate to this file too.�h j�  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKIh jw  hhubh;)��}�(hX�  The index is organized as:
- ``v2020.1.9`` (version of the core package of the index, changes periodically)
- ``JPL`` (the organization that develops/maintains the primitive)
- ``d3m.primitives.classification.logistic_regression.SKlearn`` (the python path of the actual primitive)
- ``2019.11.13`` (the version of this primitive, changes periodically)
- ``pipelines``
- ``862df0a2-2f87-450d-a6bd-24e9269a8ba6.json`` (actual pipeline description filename, changes periodically)�h]�(h/�The index is organized as:
- �����}�(h�The index is organized as:
- �h j�  hhh!NhNubjV  )��}�(h�``v2020.1.9``�h]�h/�	v2020.1.9�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubh/�D (version of the core package of the index, changes periodically)
- �����}�(h�D (version of the core package of the index, changes periodically)
- �h j�  hhh!NhNubjV  )��}�(h�``JPL``�h]�h/�JPL�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubh/�< (the organization that develops/maintains the primitive)
- �����}�(h�< (the organization that develops/maintains the primitive)
- �h j�  hhh!NhNubjV  )��}�(h�=``d3m.primitives.classification.logistic_regression.SKlearn``�h]�h/�9d3m.primitives.classification.logistic_regression.SKlearn�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubh/�- (the python path of the actual primitive)
- �����}�(h�- (the python path of the actual primitive)
- �h j�  hhh!NhNubjV  )��}�(h�``2019.11.13``�h]�h/�
2019.11.13�����}�(hhh j  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubh/�9 (the version of this primitive, changes periodically)
- �����}�(h�9 (the version of this primitive, changes periodically)
- �h j�  hhh!NhNubjV  )��}�(h�``pipelines``�h]�h/�	pipelines�����}�(hhh j  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubh/�
- �����}�(h�
- �h j�  hhh!NhNubjV  )��}�(h�-``862df0a2-2f87-450d-a6bd-24e9269a8ba6.json``�h]�h/�)862df0a2-2f87-450d-a6bd-24e9269a8ba6.json�����}�(hhh j*  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubh/�= (actual pipeline description filename, changes periodically)�����}�(h�= (actual pipeline description filename, changes periodically)�h j�  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKPh jw  hhubh;)��}�(hXo  Early on in this JSON document, you will see a list called ``steps``. This
is the actual list of primitive steps that run one after another in a
pipeline. Each step has the information about the primitive, as well as
arguments, outputs, and hyper-parameters, if any. This specific pipeline
has 5 steps (the ``d3m.primitives`` prefix is omitted in the following
list):�h]�(h/�;Early on in this JSON document, you will see a list called �����}�(h�;Early on in this JSON document, you will see a list called �h jC  hhh!NhNubjV  )��}�(h�	``steps``�h]�h/�steps�����}�(hhh jL  ubah}�(h]�h]�h]�h]�h]�uhjU  h jC  ubh/��. This
is the actual list of primitive steps that run one after another in a
pipeline. Each step has the information about the primitive, as well as
arguments, outputs, and hyper-parameters, if any. This specific pipeline
has 5 steps (the �����}�(h��. This
is the actual list of primitive steps that run one after another in a
pipeline. Each step has the information about the primitive, as well as
arguments, outputs, and hyper-parameters, if any. This specific pipeline
has 5 steps (the �h jC  hhh!NhNubjV  )��}�(h�``d3m.primitives``�h]�h/�d3m.primitives�����}�(hhh j_  ubah}�(h]�h]�h]�h]�h]�uhjU  h jC  ubh/�* prefix is omitted in the following
list):�����}�(h�* prefix is omitted in the following
list):�h jC  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKXh jw  hhubh	�bullet_list���)��}�(hhh]�(jF  )��}�(h�3``data_transformation.dataset_to_dataframe.Common``�h]�h;)��}�(hj  h]�jV  )��}�(hj  h]�h/�/data_transformation.dataset_to_dataframe.Common�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK_h j}  ubah}�(h]�h]�h]�h]�h]�uhjE  h jz  hhh!h"hNubjF  )��}�(h�,``data_transformation.column_parser.Common``�h]�h;)��}�(hj�  h]�jV  )��}�(hj�  h]�h/�(data_transformation.column_parser.Common�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK`h j�  ubah}�(h]�h]�h]�h]�h]�uhjE  h jz  hhh!h"hNubjF  )��}�(h�!``data_cleaning.imputer.SKlearn``�h]�h;)��}�(hj�  h]�jV  )��}�(hj�  h]�h/�data_cleaning.imputer.SKlearn�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKah j�  ubah}�(h]�h]�h]�h]�h]�uhjE  h jz  hhh!h"hNubjF  )��}�(h�.``classification.logistic_regression.SKlearn``�h]�h;)��}�(hj�  h]�jV  )��}�(hj�  h]�h/�*classification.logistic_regression.SKlearn�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKbh j�  ubah}�(h]�h]�h]�h]�h]�uhjE  h jz  hhh!h"hNubjF  )��}�(h�5``data_transformation.construct_predictions.Common``
�h]�h;)��}�(h�4``data_transformation.construct_predictions.Common``�h]�jV  )��}�(hj  h]�h/�0data_transformation.construct_predictions.Common�����}�(hhh j  ubah}�(h]�h]�h]�h]�h]�uhjU  h j  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKch j�  ubah}�(h]�h]�h]�h]�h]�uhjE  h jz  hhh!h"hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhjx  h!h"hK_h jw  hhubh;)��}�(hX<  Now let's take a look at the first primitive step in that pipeline. We
can find the source code of this primitive in the common-primitives repo
(`common_primitives/dataset_to_dataframe.py
<https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/dataset_to_dataframe.py>`__).
Take a look particularly at the ``produce`` method. This is essentially
what the primitive does. Try to do this for the other primitive steps in
the pipeline as well - take a cursory look at what each one essentially
does (note that for the actual classifier primitive, you should look at
the ``fit`` method as well to see how the model is trained). Primitives
whose python path suffix is ``*.Common`` is in the `common primitives <https://gitlab.com/datadrivendiscovery/common-primitives>`__
repository, and those that have a ``*.SKlearn`` suffix is in the
`sklearn-wrap <https://gitlab.com/datadrivendiscovery/sklearn-wrap>`__ repository (checkout the `dist <https://gitlab.com/datadrivendiscovery/sklearn-wrap/-/tree/dist>`__ branch,
to which primitives are being generated).�h]�(h/��Now let’s take a look at the first primitive step in that pipeline. We
can find the source code of this primitive in the common-primitives repo
(�����}�(h��Now let's take a look at the first primitive step in that pipeline. We
can find the source code of this primitive in the common-primitives repo
(�h j&  hhh!NhNubj  )��}�(h��`common_primitives/dataset_to_dataframe.py
<https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/dataset_to_dataframe.py>`__�h]�h/�)common_primitives/dataset_to_dataframe.py�����}�(h�)common_primitives/dataset_to_dataframe.py�h j/  ubah}�(h]�h]�h]�h]�h]��name�j7  j  �nhttps://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/dataset_to_dataframe.py�uhj  h j&  ubh/�#).
Take a look particularly at the �����}�(h�#).
Take a look particularly at the �h j&  hhh!NhNubjV  )��}�(h�``produce``�h]�h/�produce�����}�(hhh jE  ubah}�(h]�h]�h]�h]�h]�uhjU  h j&  ubh/�� method. This is essentially
what the primitive does. Try to do this for the other primitive steps in
the pipeline as well - take a cursory look at what each one essentially
does (note that for the actual classifier primitive, you should look at
the �����}�(h�� method. This is essentially
what the primitive does. Try to do this for the other primitive steps in
the pipeline as well - take a cursory look at what each one essentially
does (note that for the actual classifier primitive, you should look at
the �h j&  hhh!NhNubjV  )��}�(h�``fit``�h]�h/�fit�����}�(hhh jX  ubah}�(h]�h]�h]�h]�h]�uhjU  h j&  ubh/�Y method as well to see how the model is trained). Primitives
whose python path suffix is �����}�(h�Y method as well to see how the model is trained). Primitives
whose python path suffix is �h j&  hhh!NhNubjV  )��}�(h�``*.Common``�h]�h/�*.Common�����}�(hhh jk  ubah}�(h]�h]�h]�h]�h]�uhjU  h j&  ubh/� is in the �����}�(h� is in the �h j&  hhh!NhNubj  )��}�(h�P`common primitives <https://gitlab.com/datadrivendiscovery/common-primitives>`__�h]�h/�common primitives�����}�(h�common primitives�h j~  ubah}�(h]�h]�h]�h]�h]��name��common primitives�j  �8https://gitlab.com/datadrivendiscovery/common-primitives�uhj  h j&  ubh/�#
repository, and those that have a �����}�(h�#
repository, and those that have a �h j&  hhh!NhNubjV  )��}�(h�``*.SKlearn``�h]�h/�	*.SKlearn�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j&  ubh/� suffix is in the
�����}�(h� suffix is in the
�h j&  hhh!NhNubj  )��}�(h�F`sklearn-wrap <https://gitlab.com/datadrivendiscovery/sklearn-wrap>`__�h]�h/�sklearn-wrap�����}�(h�sklearn-wrap�h j�  ubah}�(h]�h]�h]�h]�h]��name�j�  j  �3https://gitlab.com/datadrivendiscovery/sklearn-wrap�uhj  h j&  ubh/� repository (checkout the �����}�(h� repository (checkout the �h j&  hhh!NhNubj  )��}�(h�J`dist <https://gitlab.com/datadrivendiscovery/sklearn-wrap/-/tree/dist>`__�h]�h/�dist�����}�(h�dist�h j�  ubah}�(h]�h]�h]�h]�h]��name�j�  j  �?https://gitlab.com/datadrivendiscovery/sklearn-wrap/-/tree/dist�uhj  h j&  ubh/�2 branch,
to which primitives are being generated).�����}�(h�2 branch,
to which primitives are being generated).�h j&  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKeh jw  hhubh;)��}�(hXX  If you're having a hard time looking for the correct source file, you can try
taking the primitive ``id`` from the primitive step description in the
pipeline, and ``grep`` for it. For example, if you were
looking for the source code of the first primitive step in this
pipeline, first look at the primitive info in that step and get its
``id``:�h]�(h/�eIf you’re having a hard time looking for the correct source file, you can try
taking the primitive �����}�(h�cIf you're having a hard time looking for the correct source file, you can try
taking the primitive �h j�  hhh!NhNubjV  )��}�(h�``id``�h]�h/�id�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubh/�: from the primitive step description in the
pipeline, and �����}�(h�: from the primitive step description in the
pipeline, and �h j�  hhh!NhNubjV  )��}�(h�``grep``�h]�h/�grep�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubh/�� for it. For example, if you were
looking for the source code of the first primitive step in this
pipeline, first look at the primitive info in that step and get its
�����}�(h�� for it. For example, if you were
looking for the source code of the first primitive step in this
pipeline, first look at the primitive info in that step and get its
�h j�  hhh!NhNubjV  )��}�(h�``id``�h]�h/�id�����}�(hhh j	  ubah}�(h]�h]�h]�h]�h]�uhjU  h j�  ubh/�:�����}�(h�:�h j�  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKsh jw  hhubh	�literal_block���)��}�(h��"primitive": {
  "id": "4b42ce1e-9b98-4a25-b68e-fad13311eb65",
  "version": "0.3.0",
  "python_path": "d3m.primitives.data_transformation.dataset_to_dataframe.Common",
  "name": "Extract a DataFrame from a Dataset"
},�h]�h/��"primitive": {
  "id": "4b42ce1e-9b98-4a25-b68e-fad13311eb65",
  "version": "0.3.0",
  "python_path": "d3m.primitives.data_transformation.dataset_to_dataframe.Common",
  "name": "Extract a DataFrame from a Dataset"
},�����}�(hhh j$  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}��	xml:space��preserve��language��default�uhj"  h!h"hKzh jw  hhubh;)��}�(h�Then, run this:�h]�h/�Then, run this:�����}�(hj;  h j9  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h jw  hhubj#  )��}�(h��git clone https://gitlab.com/datadrivendiscovery/common-primitives.git
cd common-primitives
grep -r 4b42ce1e-9b98-4a25-b68e-fad13311eb65 . | grep -F .py�h]�h/��git clone https://gitlab.com/datadrivendiscovery/common-primitives.git
cd common-primitives
grep -r 4b42ce1e-9b98-4a25-b68e-fad13311eb65 . | grep -F .py�����}�(hhh jG  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�j5  j6  j7  �shell�uhj"  h!h"hK�h jw  hhubh;)��}�(hX�  However, this series of commands assumes that you know exactly which
specific repository is the primitive's source code located in (the ``git
clone`` command). Since this is probably not the case for an arbitrarily
given primitive, there is a method on how to find out the repository URL
of any primitive, and it requires using a d3m Docker image, which is
described in the next section.�h]�(h/��However, this series of commands assumes that you know exactly which
specific repository is the primitive’s source code located in (the �����}�(h��However, this series of commands assumes that you know exactly which
specific repository is the primitive's source code located in (the �h jY  hhh!NhNubjV  )��}�(h�``git
clone``�h]�h/�	git
clone�����}�(hhh jb  ubah}�(h]�h]�h]�h]�h]�uhjU  h jY  ubh/�� command). Since this is probably not the case for an arbitrarily
given primitive, there is a method on how to find out the repository URL
of any primitive, and it requires using a d3m Docker image, which is
described in the next section.�����}�(h�� command). Since this is probably not the case for an arbitrarily
given primitive, there is a method on how to find out the repository URL
of any primitive, and it requires using a d3m Docker image, which is
described in the next section.�h jY  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h jw  hhubeh}�(h]�(�	pipelines�jg  eh]�h]�(�	pipelines��pipeline-intro�eh]�h]�uhh#h h%hhh!h"hK*�expect_referenced_by_name�}�j�  j]  s�expect_referenced_by_id�}�jg  j]  subeh}�(h]�(h�id1�eh]�h]�(�$overview of primitives and pipelines��$overview-of-primitives-and-pipelines�eh]�h]�uhh#h hhhh!h"hKj�  }�j�  hsj�  }�hhsubeh}�(h]�h]�h]�h]�h]��source�h"uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h(N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h"�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��primitives index repo�]�(j"  j�  es�refids�}�(h]�hajg  ]�j]  au�nameids�}�(j�  hj�  j�  �
primitives�jK  j  j  j�  j�  jm  jj  j*  j'  j�  jg  j�  j}  jI  jF  u�	nametypes�}�(j�  �j�  Nj�  �j  Nj�  Njm  Nj*  �j�  �j�  NjI  �uh}�(hh%j�  h%jr  hwj  h�j�  j!  jj  j�  j'  j!  jK  jE  jg  jw  j}  jw  jF  j@  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�  Ks��R��parse_messages�]�h	�system_message���)��}�(hhh]�h;)��}�(h�-Duplicate implicit target name: "primitives".�h]�h/�1Duplicate implicit target name: “primitives”.�����}�(hhh j  ubah}�(h]�h]�h]�h]�h]�uhh:h j  ubah}�(h]�h]�h]�h]�h]�jK  a�level�K�type��INFO��source�h"�line�Kuhj  h j�  hhh!h"hK%uba�transform_messages�]�(j  )��}�(hhh]�h;)��}�(hhh]�h/�JHyperlink target "overview-of-primitives-and-pipelines" is not referenced.�����}�(hhh j;  ubah}�(h]�h]�h]�h]�h]�uhh:h j8  ubah}�(h]�h]�h]�h]�h]��level�K�type�j3  �source�h"�line�Kuhj  ubj  )��}�(hhh]�h;)��}�(hhh]�h/�4Hyperlink target "pipeline-intro" is not referenced.�����}�(hhh jU  ubah}�(h]�h]�h]�h]�h]�uhh:h jR  ubah}�(h]�h]�h]�h]�h]��level�K�type�j3  �source�h"�line�K'uhj  ube�transformer�N�
decoration�Nhhub.