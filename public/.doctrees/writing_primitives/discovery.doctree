���Y      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�-Making Your Primitives Discoverable in Python�h]�h	�Text����-Making Your Primitives Discoverable in Python�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�-X:\d3m2\docs\writing_primitives\discovery.rst�hKubh)��}�(hhh]�(h)��}�(h�Primitives D3M namespace�h]�h�Primitives D3M namespace�����}�(hh1hh/hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhh,hhhh+hKubh	�	paragraph���)��}�(h�dThe :mod:`d3m.primitives` module exposes all primitives under the same
``d3m.primitives`` namespace.�h]�(h�The �����}�(h�The �hh?hhhNhNubh �pending_xref���)��}�(h�:mod:`d3m.primitives`�h]�h	�literal���)��}�(hhLh]�h�d3m.primitives�����}�(hhhhPubah}�(h ]�h"]�(�xref��py��py-mod�eh$]�h&]�h(]�uh*hNhhJubah}�(h ]�h"]�h$]�h&]�h(]��refdoc��writing_primitives/discovery��	refdomain�h[�reftype��mod��refexplicit���refwarn���	py:module�N�py:class�N�	reftarget��d3m.primitives�uh*hHhh+hKhh?ubh�. module exposes all primitives under the same
�����}�(h�. module exposes all primitives under the same
�hh?hhhNhNubhO)��}�(h�``d3m.primitives``�h]�h�d3m.primitives�����}�(hhhhvubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhh?ubh� namespace.�����}�(h� namespace.�hh?hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKhh,hhubh>)��}�(hX  This is achieved using `Python entry points <https://packaging.python.org/specifications/entry-points/>`__.
Python packages containing primitives should register them and expose
them under the common namespace by adding an entry like the following to
package's ``setup.py``:�h]�(h�This is achieved using �����}�(h�This is achieved using �hh�hhhNhNubh	�	reference���)��}�(h�S`Python entry points <https://packaging.python.org/specifications/entry-points/>`__�h]�h�Python entry points�����}�(h�Python entry points�hh�ubah}�(h ]�h"]�h$]�h&]�h(]��name��Python entry points��refuri��9https://packaging.python.org/specifications/entry-points/�uh*h�hh�ubh��.
Python packages containing primitives should register them and expose
them under the common namespace by adding an entry like the following to
package’s �����}�(h��.
Python packages containing primitives should register them and expose
them under the common namespace by adding an entry like the following to
package's �hh�hhhNhNubhO)��}�(h�``setup.py``�h]�h�setup.py�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhh�ubh�:�����}�(h�:�hh�hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK
hh,hhubh	�literal_block���)��}�(h��entry_points = {
    'd3m.primitives': [
        'primitive_namespace.PrimitiveName = my_package.my_module:PrimitiveClassName',
    ],
},�h]�h��entry_points = {
    'd3m.primitives': [
        'primitive_namespace.PrimitiveName = my_package.my_module:PrimitiveClassName',
    ],
},�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}��	xml:space��preserve��language��python�uh*h�hh+hKhh,hhubh>)��}�(h��The example above would expose the
``my_package.my_module.PrimitiveClassName`` primitive under
``d3m.primitives.primitive_namespace.PrimitiveName``.�h]�(h�#The example above would expose the
�����}�(h�#The example above would expose the
�hh�hhhNhNubhO)��}�(h�+``my_package.my_module.PrimitiveClassName``�h]�h�'my_package.my_module.PrimitiveClassName�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhh�ubh� primitive under
�����}�(h� primitive under
�hh�hhhNhNubhO)��}�(h�4``d3m.primitives.primitive_namespace.PrimitiveName``�h]�h�0d3m.primitives.primitive_namespace.PrimitiveName�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhh�ubh�.�����}�(h�.�hh�hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKhh,hhubh>)��}�(hX8  Configuring ``entry_points`` in your ``setup.py`` does not just put
primitives into a common namespace, but also helps with discovery of
your primitives on the system. Then your package with primitives just
have to be installed on the system and can be automatically discovered
and used by any other Python code.�h]�(h�Configuring �����}�(h�Configuring �hj  hhhNhNubhO)��}�(h�``entry_points``�h]�h�entry_points�����}�(hhhj   ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj  ubh�	 in your �����}�(h�	 in your �hj  hhhNhNubhO)��}�(h�``setup.py``�h]�h�setup.py�����}�(hhhj3  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj  ubhX   does not just put
primitives into a common namespace, but also helps with discovery of
your primitives on the system. Then your package with primitives just
have to be installed on the system and can be automatically discovered
and used by any other Python code.�����}�(hX   does not just put
primitives into a common namespace, but also helps with discovery of
your primitives on the system. Then your package with primitives just
have to be installed on the system and can be automatically discovered
and used by any other Python code.�hj  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKhh,hhubh	�block_quote���)��}�(hhh]�(h>)��}�(hX%  **Note:** Only primitive classes are available through the
``d3m.primitives`` namespace, no other symbols from a source
module. In the example above, only ``PrimitiveClassName`` is
available, not other symbols inside ``my_module`` (except if they
are other classes also added to entry points).�h]�(h	�strong���)��}�(h�	**Note:**�h]�h�Note:�����}�(hhhjW  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*jU  hjQ  ubh�2 Only primitive classes are available through the
�����}�(h�2 Only primitive classes are available through the
�hjQ  ubhO)��}�(h�``d3m.primitives``�h]�h�d3m.primitives�����}�(hhhjj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhjQ  ubh�N namespace, no other symbols from a source
module. In the example above, only �����}�(h�N namespace, no other symbols from a source
module. In the example above, only �hjQ  ubhO)��}�(h�``PrimitiveClassName``�h]�h�PrimitiveClassName�����}�(hhhj}  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhjQ  ubh�( is
available, not other symbols inside �����}�(h�( is
available, not other symbols inside �hjQ  ubhO)��}�(h�``my_module``�h]�h�	my_module�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhjQ  ubh�? (except if they
are other classes also added to entry points).�����}�(h�? (except if they
are other classes also added to entry points).�hjQ  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK!hjN  ubh>)��}�(hXo  **Note:** Modules under ``d3m.primitives`` are created dynamically
at run-time based on information from entry points. So some tools
(IDEs, code inspectors, etc.) might not find them because there are
no corresponding files and directories under ``d3m.primitives``
module. You have to execute Python code for modules to be available.
Static analysis cannot find them.�h]�(jV  )��}�(h�	**Note:**�h]�h�Note:�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*jU  hj�  ubh� Modules under �����}�(h� Modules under �hj�  ubhO)��}�(h�``d3m.primitives``�h]�h�d3m.primitives�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubh�� are created dynamically
at run-time based on information from entry points. So some tools
(IDEs, code inspectors, etc.) might not find them because there are
no corresponding files and directories under �����}�(h�� are created dynamically
at run-time based on information from entry points. So some tools
(IDEs, code inspectors, etc.) might not find them because there are
no corresponding files and directories under �hj�  ubhO)��}�(h�``d3m.primitives``�h]�h�d3m.primitives�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubh�g
module. You have to execute Python code for modules to be available.
Static analysis cannot find them.�����}�(h�g
module. You have to execute Python code for modules to be available.
Static analysis cannot find them.�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK'hjN  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*jL  hh,hhhh+hNubeh}�(h ]��primitives-d3m-namespace�ah"]�h$]��primitives d3m namespace�ah&]�h(]�uh*h
hhhhhh+hKubh)��}�(hhh]�(h)��}�(h�Primitives discovery on PyPI�h]�h�Primitives discovery on PyPI�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hK/ubh>)��}�(h��To facilitate automatic discovery of primitives on PyPI (or any other
compatible Python Package Index), publish a package with a keyword
``d3m_primitive`` in its ``setup.py`` configuration:�h]�(h��To facilitate automatic discovery of primitives on PyPI (or any other
compatible Python Package Index), publish a package with a keyword
�����}�(h��To facilitate automatic discovery of primitives on PyPI (or any other
compatible Python Package Index), publish a package with a keyword
�hj  hhhNhNubhO)��}�(h�``d3m_primitive``�h]�h�d3m_primitive�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj  ubh� in its �����}�(h� in its �hj  hhhNhNubhO)��}�(h�``setup.py``�h]�h�setup.py�����}�(hhhj'  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj  ubh� configuration:�����}�(h� configuration:�hj  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK1hj�  hhubh�)��}�(hX�  keywords='d3m_primitive'

**Note:** Be careful when automatically discovering, installing, and
using primitives from unknown sources. While primitives are designed
to be bootstrapable and automatically installable without human
involvement, there are no isolation mechanisms yet in place for
running potentially malicious primitives. Currently recommended way
is to use manually curated lists of known primitives.�h]�hX�  keywords='d3m_primitive'

**Note:** Be careful when automatically discovering, installing, and
using primitives from unknown sources. While primitives are designed
to be bootstrapable and automatically installable without human
involvement, there are no isolation mechanisms yet in place for
running potentially malicious primitives. Currently recommended way
is to use manually curated lists of known primitives.�����}�(hhhj@  ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}�h�h�h��python�uh*h�hh+hK5hj�  hhubeh}�(h ]��primitives-discovery-on-pypi�ah"]�h$]��primitives discovery on pypi�ah&]�h(]�uh*h
hhhhhh+hK/ubh)��}�(hhh]�(h)��}�(h�d3m.index API�h]�h�d3m.index API�����}�(hj_  hj]  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhjZ  hhhh+hKAubh>)��}�(h�KThe :mod:`d3m.index` module exposes the following Python utility functions.�h]�(h�The �����}�(h�The �hjk  hhhNhNubhI)��}�(h�:mod:`d3m.index`�h]�hO)��}�(hjv  h]�h�	d3m.index�����}�(hhhjx  ubah}�(h ]�h"]�(hZ�py��py-mod�eh$]�h&]�h(]�uh*hNhjt  ubah}�(h ]�h"]�h$]�h&]�h(]��refdoc�hg�	refdomain�j�  �reftype��mod��refexplicit���refwarn��hmNhnNho�	d3m.index�uh*hHhh+hKChjk  ubh�7 module exposes the following Python utility functions.�����}�(h�7 module exposes the following Python utility functions.�hjk  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKChjZ  hhubh)��}�(hhh]�(h)��}�(h�
``search``�h]�hO)��}�(hj�  h]�h�search�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hKFubh>)��}�(h��Returns a list of primitive paths (Python paths under ``d3m.primitives``
namespace) for all known (discoverable through entry points) primitives,
or limited by the ``primitive_path_prefix`` search argument.�h]�(h�6Returns a list of primitive paths (Python paths under �����}�(h�6Returns a list of primitive paths (Python paths under �hj�  hhhNhNubhO)��}�(h�``d3m.primitives``�h]�h�d3m.primitives�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubh�\
namespace) for all known (discoverable through entry points) primitives,
or limited by the �����}�(h�\
namespace) for all known (discoverable through entry points) primitives,
or limited by the �hj�  hhhNhNubhO)��}�(h�``primitive_path_prefix``�h]�h�primitive_path_prefix�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubh� search argument.�����}�(h� search argument.�hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKHhj�  hhubeh}�(h ]��search�ah"]�h$]��search�ah&]�h(]�uh*h
hjZ  hhhh+hKFubh)��}�(hhh]�(h)��}�(h�``get_primitive``�h]�hO)��}�(hj�  h]�h�get_primitive�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hKMubh>)��}�(h�8Loads (if not already) a primitive class and returns it.�h]�h�8Loads (if not already) a primitive class and returns it.�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKOhj�  hhubeh}�(h ]��get-primitive�ah"]�h$]��get_primitive�ah&]�h(]�uh*h
hjZ  hhhh+hKMubh)��}�(hhh]�(h)��}�(h�``get_primitive_by_id``�h]�hO)��}�(hj+  h]�h�get_primitive_by_id�����}�(hhhj-  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj)  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj&  hhhh+hKRubh>)��}�(h�OReturns a primitive class based on its ID from all currently loaded
primitives.�h]�h�OReturns a primitive class based on its ID from all currently loaded
primitives.�����}�(hjB  hj@  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKThj&  hhubeh}�(h ]��get-primitive-by-id�ah"]�h$]��get_primitive_by_id�ah&]�h(]�uh*h
hjZ  hhhh+hKRubh)��}�(hhh]�(h)��}�(h�``get_loaded_primitives``�h]�hO)��}�(hj[  h]�h�get_loaded_primitives�����}�(hhhj]  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhjY  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhjV  hhhh+hKXubh>)��}�(h�2Returns a list of all currently loaded primitives.�h]�h�2Returns a list of all currently loaded primitives.�����}�(hjr  hjp  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKZhjV  hhubeh}�(h ]��get-loaded-primitives�ah"]�h$]��get_loaded_primitives�ah&]�h(]�uh*h
hjZ  hhhh+hKXubh)��}�(hhh]�(h)��}�(h�``load_all``�h]�hO)��}�(hj�  h]�h�load_all�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hK]ubh>)��}�(h�TLoads all primitives available and populates ``d3m.primitives``
namespace with them.�h]�(h�-Loads all primitives available and populates �����}�(h�-Loads all primitives available and populates �hj�  hhhNhNubhO)��}�(h�``d3m.primitives``�h]�h�d3m.primitives�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubh�
namespace with them.�����}�(h�
namespace with them.�hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK_hj�  hhubeh}�(h ]��load-all�ah"]�h$]��load_all�ah&]�h(]�uh*h
hjZ  hhhh+hK]ubh)��}�(hhh]�(h)��}�(h�``register_primitive``�h]�hO)��}�(hj�  h]�h�register_primitive�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hKcubh>)��}�(h�9Registers a primitive under ``d3m.primitives`` namespace.�h]�(h�Registers a primitive under �����}�(h�Registers a primitive under �hj�  hhhNhNubhO)��}�(h�``d3m.primitives``�h]�h�d3m.primitives�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubh� namespace.�����}�(h� namespace.�hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKehj�  hhubh>)��}�(h��This is useful to register primitives not necessary installed on the
system or which are generated at runtime. It is also useful for testing
purposes.�h]�h��This is useful to register primitives not necessary installed on the
system or which are generated at runtime. It is also useful for testing
purposes.�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKghj�  hhubeh}�(h ]��register-primitive�ah"]�h$]��register_primitive�ah&]�h(]�uh*h
hjZ  hhhh+hKcubh)��}�(hhh]�(h)��}�(h�``discover``�h]�hO)��}�(hj!  h]�h�discover�����}�(hhhj#  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj  hhhh+hKlubh>)��}�(h�=Returns package names from PyPi which provide D3M primitives.�h]�h�=Returns package names from PyPi which provide D3M primitives.�����}�(hj8  hj6  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKnhj  hhubh>)��}�(h�MThis is determined by them having a ``d3m_primitive`` among package
keywords.�h]�(h�$This is determined by them having a �����}�(h�$This is determined by them having a �hjD  hhhNhNubhO)��}�(h�``d3m_primitive``�h]�h�d3m_primitive�����}�(hhhjM  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhjD  ubh� among package
keywords.�����}�(h� among package
keywords.�hjD  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKphj  hhubeh}�(h ]��discover�ah"]�h$]��discover�ah&]�h(]�uh*h
hjZ  hhhh+hKlubeh}�(h ]��d3m-index-api�ah"]�h$]��d3m.index api�ah&]�h(]�uh*h
hhhhhh+hKAubh)��}�(hhh]�(h)��}�(h�Command line�h]�h�Command line�����}�(hj{  hjy  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhjv  hhhh+hKtubh>)��}�(h��The :mod:`d3m.index` module also provides a command line interface by
running ``python3 -m d3m primitive``. The following commands are currently
available.�h]�(h�The �����}�(h�The �hj�  hhhNhNubhI)��}�(h�:mod:`d3m.index`�h]�hO)��}�(hj�  h]�h�	d3m.index�����}�(hhhj�  ubah}�(h ]�h"]�(hZ�py��py-mod�eh$]�h&]�h(]�uh*hNhj�  ubah}�(h ]�h"]�h$]�h&]�h(]��refdoc�hg�	refdomain�j�  �reftype��mod��refexplicit���refwarn��hmNhnNho�	d3m.index�uh*hHhh+hKvhj�  ubh�: module also provides a command line interface by
running �����}�(h�: module also provides a command line interface by
running �hj�  hhhNhNubhO)��}�(h�``python3 -m d3m primitive``�h]�h�python3 -m d3m primitive�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubh�1. The following commands are currently
available.�����}�(h�1. The following commands are currently
available.�hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKvhjv  hhubh>)��}�(h�bUse ``-h`` or ``--help`` argument to obtain more information about each
command and its arguments.�h]�(h�Use �����}�(h�Use �hj�  hhhNhNubhO)��}�(h�``-h``�h]�h�-h�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubh� or �����}�(h� or �hj�  hhhNhNubhO)��}�(h�
``--help``�h]�h�--help�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj�  ubh�J argument to obtain more information about each
command and its arguments.�����}�(h�J argument to obtain more information about each
command and its arguments.�hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hKzhjv  hhubh)��}�(hhh]�(h)��}�(h�#``python3 -m d3m primitive search``�h]�hO)��}�(hj  h]�h�python3 -m d3m primitive search�����}�(hhhj
  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj  hhhh+hK~ubh>)��}�(h�lSearches locally available primitives. Lists registered Python paths for
primitives installed on the system.�h]�h�lSearches locally available primitives. Lists registered Python paths for
primitives installed on the system.�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK�hj  hhubeh}�(h ]��python3-m-d3m-primitive-search�ah"]�h$]��python3 -m d3m primitive search�ah&]�h(]�uh*h
hjv  hhhh+hK~ubh)��}�(hhh]�(h)��}�(h�%``python3 -m d3m primitive discover``�h]�hO)��}�(hj8  h]�h�!python3 -m d3m primitive discover�����}�(hhhj:  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhj6  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj3  hhhh+hK�ubh>)��}�(h�^Discovers primitives available on PyPi. Lists package names containing
D3M primitives on PyPi.�h]�h�^Discovers primitives available on PyPi. Lists package names containing
D3M primitives on PyPi.�����}�(hjO  hjM  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK�hj3  hhubeh}�(h ]�� python3-m-d3m-primitive-discover�ah"]�h$]��!python3 -m d3m primitive discover�ah&]�h(]�uh*h
hjv  hhhh+hK�ubh)��}�(hhh]�(h)��}�(h�%``python3 -m d3m primitive describe``�h]�hO)��}�(hjh  h]�h�!python3 -m d3m primitive describe�����}�(hhhjj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hNhjf  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhjc  hhhh+hK�ubh>)��}�(h�BGenerates a primitive annotation of a primitive from its metadata.�h]�h�BGenerates a primitive annotation of a primitive from its metadata.�����}�(hj  hj}  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h=hh+hK�hjc  hhubeh}�(h ]�� python3-m-d3m-primitive-describe�ah"]�h$]��!python3 -m d3m primitive describe�ah&]�h(]�uh*h
hjv  hhhh+hK�ubeh}�(h ]��command-line�ah"]�h$]��command line�ah&]�h(]�uh*h
hhhhhh+hKtubeh}�(h ]��-making-your-primitives-discoverable-in-python�ah"]�h$]��-making your primitives discoverable in python�ah&]�h(]�uh*h
hhhhhh+hKubah}�(h ]�h"]�h$]�h&]�h(]��source�h+uh*h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j�  j�  jW  jT  js  jp  j�  j�  j#  j   jS  jP  j�  j�  j�  j�  j  j  jk  jh  j�  j�  j0  j-  j`  j]  j�  j�  u�	nametypes�}�(j�  Nj�  NjW  Njs  Nj�  Nj#  NjS  Nj�  Nj�  Nj  Njk  Nj�  Nj0  Nj`  Nj�  Nuh }�(j�  hj�  h,jT  j�  jp  jZ  j�  j�  j   j�  jP  j&  j�  jV  j�  j�  j  j�  jh  j  j�  jv  j-  j  j]  j3  j�  jc  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.