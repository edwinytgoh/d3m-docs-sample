��V      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�"Setting Up a Local D3M Environment�h]�h	�Text����"Setting Up a Local D3M Environment�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�9X:\d3m2\docs\writing_primitives\local_d3m_environment.rst�hKubh	�	paragraph���)��}�(hXp  In order to run a pipeline, you must have a Python environment where the
d3m core package is installed, as well as the packages of the primitives
installed as well. While it is possible to setup a Python virtual
environment and install the packages them through ``pip``, in this
tutorial, we're going to use the d3m Docker images instead (in many
cases, even beyond this tutorial, this will save you a lot of time and
effort trying to find the any missing primitive packages, manually
installing them, and troubleshooting installation errors). So, make sure
`Docker <https://docs.docker.com/>`__ is installed in your system.�h]�(hX  In order to run a pipeline, you must have a Python environment where the
d3m core package is installed, as well as the packages of the primitives
installed as well. While it is possible to setup a Python virtual
environment and install the packages them through �����}�(hX  In order to run a pipeline, you must have a Python environment where the
d3m core package is installed, as well as the packages of the primitives
installed as well. While it is possible to setup a Python virtual
environment and install the packages them through �hh.hhhNhNubh	�literal���)��}�(h�``pip``�h]�h�pip�����}�(hhhh9ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hh.ubhX#  , in this
tutorial, we’re going to use the d3m Docker images instead (in many
cases, even beyond this tutorial, this will save you a lot of time and
effort trying to find the any missing primitive packages, manually
installing them, and troubleshooting installation errors). So, make sure
�����}�(hX!  , in this
tutorial, we're going to use the d3m Docker images instead (in many
cases, even beyond this tutorial, this will save you a lot of time and
effort trying to find the any missing primitive packages, manually
installing them, and troubleshooting installation errors). So, make sure
�hh.hhhNhNubh	�	reference���)��}�(h�%`Docker <https://docs.docker.com/>`__�h]�h�Docker�����}�(h�Docker�hhNubah}�(h ]�h"]�h$]�h&]�h(]��name�hV�refuri��https://docs.docker.com/�uh*hLhh.ubh� is installed in your system.�����}�(h� is installed in your system.�hh.hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh-)��}�(hX  You can find the list of D3M docker images `here <https://datadrivendiscovery.org/docker.html>`__.
The one we're going to use in this tutorial is the v2020.1.9
primitives image (feel free to use whatever the latest one instead
though - just modify the ``v2020.1.9`` part accordingly):�h]�(h�+You can find the list of D3M docker images �����}�(h�+You can find the list of D3M docker images �hhkhhhNhNubhM)��}�(h�6`here <https://datadrivendiscovery.org/docker.html>`__�h]�h�here�����}�(h�here�hhtubah}�(h ]�h"]�h$]�h&]�h(]��name�h|h^�+https://datadrivendiscovery.org/docker.html�uh*hLhhkubh��.
The one we’re going to use in this tutorial is the v2020.1.9
primitives image (feel free to use whatever the latest one instead
though - just modify the �����}�(h��.
The one we're going to use in this tutorial is the v2020.1.9
primitives image (feel free to use whatever the latest one instead
though - just modify the �hhkhhhNhNubh8)��}�(h�``v2020.1.9``�h]�h�	v2020.1.9�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hhkubh� part accordingly):�����}�(h� part accordingly):�hhkhhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh	�literal_block���)��}�(h�fdocker pull registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9�h]�h�fdocker pull registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}��	xml:space��preserve��language��shell�uh*h�hh+hKhhhhubh-)��}�(h��Once you have downloaded the image, we can finally run the d3m package
(and hence run a pipeline). Before running a pipeline though, let's
first try to get a list of what primitives are installed in the image's
Python environment:�h]�h��Once you have downloaded the image, we can finally run the d3m package
(and hence run a pipeline). Before running a pipeline though, let’s
first try to get a list of what primitives are installed in the image’s
Python environment:�����}�(hh�hh�hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh�)��}�(h��docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m primitive search�h]�h��docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m primitive search�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}�h�h�h��shell�uh*h�hh+hKhhhhubh-)��}�(h�\You should get a big list of primitives. All of the known primitives to
D3M should be there.�h]�h�\You should get a big list of primitives. All of the known primitives to
D3M should be there.�����}�(hh�hh�hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK hhhhubh-)��}�(h��You can also run the docker container in interactive mode (to run
commands as if you have logged into the container machine provides) by
using the ``-it`` option:�h]�(h��You can also run the docker container in interactive mode (to run
commands as if you have logged into the container machine provides) by
using the �����}�(h��You can also run the docker container in interactive mode (to run
commands as if you have logged into the container machine provides) by
using the �hh�hhhNhNubh8)��}�(h�``-it``�h]�h�-it�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hh�ubh� option:�����}�(h� option:�hh�hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK#hhhhubh�)��}�(h�ndocker run --rm -it registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9�h]�h�ndocker run --rm -it registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9�����}�(hhhj
  ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}�h�h�h��shell�uh*h�hh+hK'hhhhubh-)��}�(hXZ  The previous section mentions a method of determining where the source
code of an arbitrarily given primitive can be found. We can do this
using the d3m python package within a d3m docker container. First get the
``python_path`` of the primitive step (see the JSON snippet above of the
primitive's info from the pipeline). Then, run this command:�h]�(h��The previous section mentions a method of determining where the source
code of an arbitrarily given primitive can be found. We can do this
using the d3m python package within a d3m docker container. First get the
�����}�(h��The previous section mentions a method of determining where the source
code of an arbitrarily given primitive can be found. We can do this
using the d3m python package within a d3m docker container. First get the
�hj  hhhNhNubh8)��}�(h�``python_path``�h]�h�python_path�����}�(hhhj%  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj  ubh�x of the primitive step (see the JSON snippet above of the
primitive’s info from the pipeline). Then, run this command:�����}�(h�v of the primitive step (see the JSON snippet above of the
primitive's info from the pipeline). Then, run this command:�hj  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK+hhhhubh�)��}�(h��docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m primitive describe d3m.primitives.data_transformation.dataset_to_dataframe.Common�h]�h��docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m primitive describe d3m.primitives.data_transformation.dataset_to_dataframe.Common�����}�(hhhj>  ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}�h�h�h��shell�uh*h�hh+hK1hhhhubh-)��}�(h��Near the top of the huge JSON string describing the primitive, you'll see
``"source"``, and inside it, ``"uris"``. To help read the JSON, you can use
the ``jq`` utility:�h]�(h�LNear the top of the huge JSON string describing the primitive, you’ll see
�����}�(h�JNear the top of the huge JSON string describing the primitive, you'll see
�hjP  hhhNhNubh8)��}�(h�``"source"``�h]�h�"source"�����}�(hhhjY  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hjP  ubh�, and inside it, �����}�(h�, and inside it, �hjP  hhhNhNubh8)��}�(h�
``"uris"``�h]�h�"uris"�����}�(hhhjl  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hjP  ubh�). To help read the JSON, you can use
the �����}�(h�). To help read the JSON, you can use
the �hjP  hhhNhNubh8)��}�(h�``jq``�h]�h�jq�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hjP  ubh�	 utility:�����}�(h�	 utility:�hjP  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK5hhhhubh�)��}�(h��docker run --rm -it registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9
python3 -m d3m primitive describe d3m.primitives.data_transformation.dataset_to_dataframe.Common | jq .source.uris�h]�h��docker run --rm -it registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9
python3 -m d3m primitive describe d3m.primitives.data_transformation.dataset_to_dataframe.Common | jq .source.uris�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}�h�h�h��shell�uh*h�hh+hK9hhhhubh-)��}�(hX�  This should give the URI of the git repo where the source code of that primitive can be found. Also, You
can also substitute the primitive ``id`` for the ``python_path`` in that
command, but the command usually returns a result faster if you provide
the ``python_path``. Note also that you can only do this for primitives
that have been submitted for a particular image (primitives that are
contained in the `primitives index repo`_).�h]�(h��This should give the URI of the git repo where the source code of that primitive can be found. Also, You
can also substitute the primitive �����}�(h��This should give the URI of the git repo where the source code of that primitive can be found. Also, You
can also substitute the primitive �hj�  hhhNhNubh8)��}�(h�``id``�h]�h�id�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj�  ubh�	 for the �����}�(h�	 for the �hj�  hhhNhNubh8)��}�(h�``python_path``�h]�h�python_path�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj�  ubh�U in that
command, but the command usually returns a result faster if you provide
the �����}�(h�U in that
command, but the command usually returns a result faster if you provide
the �hj�  hhhNhNubh8)��}�(h�``python_path``�h]�h�python_path�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj�  ubh��. Note also that you can only do this for primitives
that have been submitted for a particular image (primitives that are
contained in the �����}�(h��. Note also that you can only do this for primitives
that have been submitted for a particular image (primitives that are
contained in the �hj�  hhhNhNubh	�problematic���)��}�(h�`primitives index repo`_�h]�h�`primitives index repo`_�����}�(hhhj�  ubah}�(h ]��id2�ah"]�h$]�h&]�h(]��refid��id1�uh*j�  hj�  hhhNhNubh�).�����}�(h�).�hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK>hhhhubh-)��}�(hX%  It can be obscure at first how to use the d3m python package, but you can
always access the help string for each d3m command at every level of the
command chain by using the ``-h`` flag. This is useful especially for
the getting a list of all the possible arguments for the ``runtime``
module.�h]�(h��It can be obscure at first how to use the d3m python package, but you can
always access the help string for each d3m command at every level of the
command chain by using the �����}�(h��It can be obscure at first how to use the d3m python package, but you can
always access the help string for each d3m command at every level of the
command chain by using the �hj
  hhhNhNubh8)��}�(h�``-h``�h]�h�-h�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj
  ubh�^ flag. This is useful especially for
the getting a list of all the possible arguments for the �����}�(h�^ flag. This is useful especially for
the getting a list of all the possible arguments for the �hj
  hhhNhNubh8)��}�(h�``runtime``�h]�h�runtime�����}�(hhhj&  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj
  ubh�
module.�����}�(h�
module.�hj
  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKEhhhhubh�)��}�(hX  docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m primitive -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m runtime -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m runtime fit-score -h�h]�hX  docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m primitive -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m runtime -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m runtime fit-score -h�����}�(hhhj?  ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}�h�h�h��shell�uh*h�hh+hKKhhhhubh-)��}�(hX�  One last point before we try running a pipeline. The docker container
must be able to access the dataset location and the pipeline location
from the host filesystem. We can do this by `bind-mounting
<https://docs.docker.com/storage/bind-mounts/>`__ a host directory that
contains both the ``datasets`` repo and the ``primitives`` index repo to
a container directory. Git clone these repos, and also make another empty directory called
``pipeline-outputs``. Now, if your directory structure looks like this::�h]�(h��One last point before we try running a pipeline. The docker container
must be able to access the dataset location and the pipeline location
from the host filesystem. We can do this by �����}�(h��One last point before we try running a pipeline. The docker container
must be able to access the dataset location and the pipeline location
from the host filesystem. We can do this by �hjQ  hhhNhNubhM)��}�(h�@`bind-mounting
<https://docs.docker.com/storage/bind-mounts/>`__�h]�h�bind-mounting�����}�(h�bind-mounting�hjZ  ubah}�(h ]�h"]�h$]�h&]�h(]��name�jb  h^�,https://docs.docker.com/storage/bind-mounts/�uh*hLhjQ  ubh�) a host directory that
contains both the �����}�(h�) a host directory that
contains both the �hjQ  hhhNhNubh8)��}�(h�``datasets``�h]�h�datasets�����}�(hhhjp  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hjQ  ubh� repo and the �����}�(h� repo and the �hjQ  hhhNhNubh8)��}�(h�``primitives``�h]�h�
primitives�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hjQ  ubh�j index repo to
a container directory. Git clone these repos, and also make another empty directory called
�����}�(h�j index repo to
a container directory. Git clone these repos, and also make another empty directory called
�hjQ  hhhNhNubh8)��}�(h�``pipeline-outputs``�h]�h�pipeline-outputs�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hjQ  ubh�3. Now, if your directory structure looks like this:�����}�(h�3. Now, if your directory structure looks like this:�hjQ  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKRhhhhubh�)��}�(h�P/home/foo/d3m
├── datasets
├── pipeline-outputs
└── primitives�h]�h�P/home/foo/d3m
├── datasets
├── pipeline-outputs
└── primitives�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�uh*h�hh+hKZhhhhubh-)��}�(h��Then you'll want to bind-mount ``/home/foo/d3m`` to a directory in the
container, say ``/mnt/d3m``. You can specify this mapping in the docker
command itself:�h]�(h�!Then you’ll want to bind-mount �����}�(h�Then you'll want to bind-mount �hj�  hhhNhNubh8)��}�(h�``/home/foo/d3m``�h]�h�/home/foo/d3m�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj�  ubh�& to a directory in the
container, say �����}�(h�& to a directory in the
container, say �hj�  hhhNhNubh8)��}�(h�``/mnt/d3m``�h]�h�/mnt/d3m�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj�  ubh�<. You can specify this mapping in the docker
command itself:�����}�(h�<. You can specify this mapping in the docker
command itself:�hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK_hhhhubh�)��}�(h��docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2019.11.10 \
    ls /mnt/d3m�h]�h��docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2019.11.10 \
    ls /mnt/d3m�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}�h�h�h��shell�uh*h�hh+hKchhhhubh-)��}�(hX�  If you're reading this tutorial from a text editor, it might be a good
idea at this point to find and replace ``/home/foo/d3m`` with the actual
path in your system where the ``datasets``, ``pipeline-outputs``, and
``primitives`` directories are all located. This will make it easier for
you to just copy and paste the commands from here on out, instead of
changing the faux path every time.�h]�(h�pIf you’re reading this tutorial from a text editor, it might be a good
idea at this point to find and replace �����}�(h�nIf you're reading this tutorial from a text editor, it might be a good
idea at this point to find and replace �hj  hhhNhNubh8)��}�(h�``/home/foo/d3m``�h]�h�/home/foo/d3m�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj  ubh�/ with the actual
path in your system where the �����}�(h�/ with the actual
path in your system where the �hj  hhhNhNubh8)��}�(h�``datasets``�h]�h�datasets�����}�(hhhj   ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj  ubh�, �����}�(h�, �hj  hhhNhNubh8)��}�(h�``pipeline-outputs``�h]�h�pipeline-outputs�����}�(hhhj3  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj  ubh�, and
�����}�(h�, and
�hj  hhhNhNubh8)��}�(h�``primitives``�h]�h�
primitives�����}�(hhhjF  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj  ubh�� directories are all located. This will make it easier for
you to just copy and paste the commands from here on out, instead of
changing the faux path every time.�����}�(h�� directories are all located. This will make it easier for
you to just copy and paste the commands from here on out, instead of
changing the faux path every time.�hj  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKkhhhhubeh}�(h ]��"setting-up-a-local-d3m-environment�ah"]�h$]��"setting up a local d3m environment�ah&]�h(]�uh*h
hhhhhh+hKubah}�(h ]�h"]�h$]�h&]�h(]��source�h+uh*h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��primitives index repo�]�hM)��}�(hj�  h]�h�primitives index repo�����}�(h�primitives index repo�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]��name��primitives index repo��refname�j�  uh*hLhj�  ubas�refids�}��nameids�}�jd  ja  s�	nametypes�}�jd  Nsh }�(ja  hj�  h	�system_message���)��}�(hhh]�h-)��}�(hhh]�h�-Unknown target name: "primitives index repo".�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hj�  ubah}�(h ]�j�  ah"]�h$]�h&]�h(]�j�  a�level�K�type��ERROR��source�h+�line�K>uh*j�  ubj�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�  Ks��R��parse_messages�]��transform_messages�]�j�  a�transformer�N�
decoration�Nhhub.